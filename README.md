Test task for some work

You	are	offered to	create	a syntactic	application	– a	blog.
Blog	contains	Posts.	Each	Post belongs	to	a	certain	Category.	Each Post is	tagged	with one	or	more Tags and	can	
be	commented	by	both	registered	and	anonymous	visitors.
Categories should	be	organized	into	a	tree	structure.
The	following user roles should	be	implemented:
1. Visitor – not-authenticated user of the	blog.
2. Registered user – a Visitor signed	in	to	some	pre-registered	account within	the	website.
3. Administrator – the author of the blog	with	all	the	privileges	granted.
Common	use	cases	are	the	following:
1. Administrator can	create,	edit and delete Posts.
When	writing	a	post there	should	be	an	ability	to	select	the	Category (one	of	those	added	at	the	
deployment	stage)	and	choose	Tags (either	typed	in	or	selected	from	the	existing	ones). A single Post
can	tagged by	one	or	more	Tags.
2. Visitor can browse the blog	posts (filtering	and	sorting	modes	can	be	combined	in	any	reasonable	way):
a. Sorting	by	date.
©	UNIQ	SYSTEMS,	2016 Page 2/2
Grow your ides into a successful product
b. Filtering Posts containing certain text	(both	in	subject	and	body	part).	The text matches should
be highlighted	within	the	search	results.
c. Filtering	Posts by Category.
d. Filtering Posts by Tags.
3. Visitor	can sign up. That moves him	into	the	category	of Registered	users. The	first	registered	user	
becomes	the	Administrator.	Every	user	should	receive	sign up	notification	via	email.
4. Visitor can	authenticate	via	email/password	or	using	some	social	network auth service.
5. A	Visitor and	Registered user can	comment the	posts.	Commenting should	be	protected	by	CAPTCHA for	
Visitors.
6. Administrator should	moderate	the	comments:	accept, decline and remove.
The	product	should	be	fully	functional,	deployed	via	traditional	Rails-style	and	should	be	provided	(at	least	
partly)	with	the	test	code	for	controllers,	models,	helpers	and	the integration	tests.
Notices
1. The	visual	design	isn’t	being	taken	into	any	account,	unless	its	lack	renders	the	application	unusable.
2. You are encouraged	to	use	any	open	source	plugins,	libraries	and	extensions