class UserMailer < ActionMailer::Base
  default from: "testjob@mail.ru"


  def new_registration(user)
    @user = user
    @url = 'http://emample.com/login'
    mail(to: @user.email, subject: 'Welcome to Site')
  end

end
