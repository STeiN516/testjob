class RegistrationController < Devise::RegistrationsController

  def create
    super
    begin
      if @user.present?
        UserMailer.new_registration(@user).deliver!
      end
    rescue Exception => e
      logger.error e.message
    end
  end

end