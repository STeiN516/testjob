class AdminController < ApplicationController
  before_action :authenticate_user!

  before_action :check_admin

  def check_admin
    if current_user
      render(:file => File.join(Rails.root, 'public/403.html'), :status => 403, :layout => false) unless current_user.admin?(current_user)
    end
  end
end