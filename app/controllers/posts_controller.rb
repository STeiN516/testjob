class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!, except: [:index, :show, :search]


  # GET /posts
  # GET /posts.json
  def index
    sort = :desc
    sort = :asc if params[:sort] == 'asc'

    @posts = Post.order(created_at: sort).page(params[:page])

    @posts = Post.category_tags(params[:tag_ids], params[:category_id])
                 .order(created_at: :desc).page(params[:page]) if params[:tag_ids].present? and params[:category_id].present?

    @posts = Post.tags(params[:tag_ids])
                 .order(created_at: :desc).page(params[:page]) if params[:tag_ids].present? and not params[:category_id].present?

    @posts = Post.category(params[:category_id])
                 .order(created_at: :desc).page(params[:page]) if not params[:tag_ids].present? and params[:category_id].present?
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new_with_tag(post_params, params[:post][:new_tag])

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if Post.update_with_tag(@post, post_params, params[:post][:new_tag])
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search
    search = '%' + params[:search] + '%'
    @posts = Post.where('caption like ? or article like ?', search, search) if params[:search].present?
    @search = params[:search]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:caption, :article, :category_id, :tag_ids => [])
    end
end
