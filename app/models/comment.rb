class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user

  apply_simple_captcha

  validates :text, presence: true
  validates :user_name, presence: true
  validates :post_id, presence: true

  enum status: {accepting: 0, declining: 1, removal: 2}

end
