class Category < ActiveRecord::Base
  has_many :posts

  belongs_to :parent, class_name: "Category"
  has_many :children, class_name: "Category", foreign_key: "parent_id"

  validates :name, presence: true, length: {in: 2..255}, uniqueness: true

  def parent_name
    parent.try(:name)
  end
end
