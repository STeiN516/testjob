class Post < ActiveRecord::Base
  paginates_per 10
  belongs_to :category
  has_many :comments
  has_and_belongs_to_many :tags

  validates_associated :tags

  validates :caption, presence: true, length: {in: 2..255}, uniqueness: true
  validates :article, presence: true
  validates :category_id, presence: true

  scope :category, -> (category_id) {where category_id: category_id}
  scope :tags, -> (tag_ids) {where('tag_id in (?)', tag_ids).joins(:tags)}
  scope :category_tags, -> (tag_ids, category_id) {
      where('tag_id in (?) and category_id = ?', tag_ids, category_id).joins(:tags) }


  def self.new_with_tag (params, tags_new)
    @post = Post.new(params)
    unless tags_new.nil?
      tags_new.split(',').each do |tag|
        @post.tags << Tag.create(name: tag.strip)
      end
    end
    @post
  end

  def self.update_with_tag (post, params, tags_new)
    post.update(params)
    unless tags_new.nil?
      tags_new.split(',').each do |tag|
        post.tags << Tag.create(name: tag.strip)
      end
    end
    post.save
  end
end
