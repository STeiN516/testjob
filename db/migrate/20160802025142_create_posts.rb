class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :caption
      t.text :article
      t.integer :category_id

      t.timestamps
    end
  end
end
