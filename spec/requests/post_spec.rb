require 'rails_helper'

RSpec.describe Post, type: :request do

  it "root" do
    get root_path
    expect(response.status).to eq(200)
  end

  it "index" do
    get posts_path
    expect(response.status).to eq(200)
  end


  it "show" do
    post = FactoryGirl.create(:post)
    post.category = FactoryGirl.create(:category)
    get post_path post
    expect(response.status).to eq(200)

    expect(response.body).to include('simple_captcha')
  end

  it "show auth user" do
    user = FactoryGirl.create(:user, id: 0)
    post_via_redirect user_session_path, 'user[email]' => user.email,
                      'user[password]' => user.password

    post = FactoryGirl.create(:post)
    post.category = FactoryGirl.create(:category)
    get post_path post
    expect(response.status).to eq(200)

    expect(response.body).not_to include('simple_captcha')
  end


  it "search" do
    FactoryGirl.create(:post, article: 'old')
    FactoryGirl.create(:post, caption: 'new')
    FactoryGirl.create(:post, article: 'new', caption: 'cap2')
    get search_path, {:search => 'new'}
    expect(response.status).to eq(200)
    expect(response.body).to include('new')
    expect(response.body).not_to include('old')
  end


  it "new" do
    get new_post_path
    expect(response.status).to eq(302)
  end


  it "update" do
    post = FactoryGirl.create(:post)
    get edit_post_path post
    expect(response.status).to eq(302)
  end


  it "admin new post" do
    user = FactoryGirl.create(:user, id: 0)
    post_via_redirect user_session_path, 'user[email]' => user.email,
                      'user[password]' => user.password

    get new_post_path
    expect(response.status).to eq(200)
  end

  it "other not new post" do
    FactoryGirl.create(:user, id: 0)
    user = FactoryGirl.create(:user, id: 1, email: 'second@sec.com')
    post_via_redirect user_session_path, 'user[email]' => user.email,
                      'user[password]' => user.password

    get new_post_path
    expect(response.status).to eq(200)
  end


end