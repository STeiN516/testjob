require 'rails_helper'

RSpec.describe Tag, type: :model do

  it "create Tag" do
    tag = FactoryGirl.create(:tag)
    expect(Tag.last).to eq(tag)
  end

  it "validation" do
    FactoryGirl.create(:tag)
    tag = FactoryGirl.build_stubbed(:tag)
    expect(tag).not_to be_valid
  end

end
