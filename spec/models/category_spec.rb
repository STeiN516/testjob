require 'rails_helper'

RSpec.describe Category, type: :model do
  it "create Category" do
    category = FactoryGirl.create(:category)
    expect(Category.last).to eq(category)
  end


  it "have parent" do
    FactoryGirl.create(:category)
    children = FactoryGirl.create(:category, id: 1, name: "children", parent_id: 0)

    expect(children.parent_name).to eq("root")
  end


  it "validation" do
    expect(FactoryGirl.build_stubbed(:category, name: nil)).not_to be_valid
  end


end
