require 'rails_helper'

RSpec.describe User, type: :model do

  it "create User" do
    user = FactoryGirl.create(:user)
    expect(User.last).to eq(user)
  end

  it "admin?" do
    user_first =FactoryGirl.create(:user)
    user_second = FactoryGirl.create(:user, email: 'second@sec.com')
    expect(user_first.admin?(user_first)).to be true
    expect(user_second.admin?(user_second)).to be false
  end

end
