require 'rails_helper'

RSpec.describe Comment, type: :model do
  it "create Comment" do
    comment = FactoryGirl.create(:comment)
    expect(Comment.last).to eq(comment)
  end

end
