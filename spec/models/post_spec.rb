require 'rails_helper'

RSpec.describe Post, type: :model do

  it "create Post" do
    # l = Post.create!(caption: "cap", category_id: 1, article: "article")
    l = FactoryGirl.create(:post)
    expect(Post.last).to eq(l)
  end


  it "create Post with tags" do
    tag1 = FactoryGirl.create(:tag, id: 1, name: "tag1")
    tag2 = FactoryGirl.create(:tag, id: 2, name: "tag2")

    post = Post.new_with_tag( {caption: "cap", category_id: 2, article: "article",
                                 tag_ids: ["1", "2"] }, "qwe, asd ertgt")

    expect(post.tags).to include(tag1)
    expect(post.tags).to include(tag2)
    expect(post.tags[2].name).to eq("qwe")
    expect(post.tags[3].name).to eq("asd ertgt")
    expect(post.tags.size).to eq(4)

  end

  it "not valid create Post with non-unique tags" do
    post = Post.new_with_tag( {caption: "cap", category_id: 1, article: "article" }, "qwe, qwe, ertgt")

    expect(post).to_not be_valid
  end

  it "update Post with tag" do
    tag1 = FactoryGirl.create(:tag, id: 1, name: "tag1")
    tag2 = FactoryGirl.create(:tag, id: 2, name: "tag2")
    tag3 = FactoryGirl.create(:tag, id: 3, name: "tag3")

    post = Post.create!( {caption: "cap", category_id: 2, article: "article",
                                 tag_ids: ["1", "2"] })

    Post.update_with_tag(post, {caption: "cap2", category_id: 3, article: "edit article",
                                  tag_ids: ["1", "3"] }, "new tag")


    expect(post.tags).to include(tag1)
    expect(post.tags).to include(tag3)
    expect(post.tags).to_not include(tag2)
    expect(post.tags[2].name).to eq("new tag")
    expect(post.tags.size).to eq(3)
    expect(post.caption).to eq("cap2")
    expect(post.category_id).to eq(3)
    expect(post.article).to eq("edit article")
  end

  it "scope category" do
    post = FactoryGirl.create(:post)

    expect(Post.category(0)).to include(post)
    expect(Post.category(1)).not_to include(post)
  end


  it "scope tags" do
    post = FactoryGirl.create(:post)
    post.tags << FactoryGirl.create(:tag, id: 0)

    expect(Post.tags([0])).to include(post)
    expect(Post.tags([1])).not_to include(post)
  end

  it "scope category and tags" do
    post = FactoryGirl.create(:post)
    post.tags << FactoryGirl.create(:tag, id: 0)

    expect(Post.category_tags(0, [0])).to include(post)
    expect(Post.category_tags(0, [1])).not_to include(post)
    expect(Post.category_tags(1, [0])).not_to include(post)
  end

end





