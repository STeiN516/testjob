require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  it "UserMailer" do
    user = FactoryGirl.create(:user)
    email = UserMailer.new_registration(user).deliver

    expect(ActionMailer::Base.deliveries).not_to be_empty
    expect(email.to).to eq([user.email])
    expect(email.from).to eq(['testjob@mail.ru'])
    expect(email.subject).to eq('Welcome to Site')
    expect(email.body.encoded).to match(user.email)
  end
end
